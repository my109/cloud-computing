# Introduction to Cloud Computing

These materials accompany the DuPRI "Intro to Cloud Computing" workshop. Included is an overview of cloud computing services available both inside and outside of Duke, along with information on how to access those services. R and Stata code demonstrating the benefits of parallelizing code on VMs and showing benchmarks is also included. If you have any questions, contact Mark Yacoub at <mark.yacoub@duke.edu>.

* bootstrapping.R - R bootstrapping example with parallelization
* imputation.R- R multiple imputation example with parallelization
* simulation.R - R simulation example with parallelization
* imputation.do - Stata multiple imputation example
* GSS2016.DTA - 2016 GSS data set
* GSS_Codebook.pdf - GSS codebook
* Cloud_Computing.Rproj - RStudio Project file
* VM_creation.pdf - Instructions for requesting a Research Toolkits or VCM VM
* VM_access.pdf - Instructions for accessing Duke VMs and storage
* AWS_instructions.pdf - Instructions for getting started with Amazon Web Services VMs
* GCP_instructions.pdf - Instructions for getting started with Google Cloud Platform VMs
* Dupri_Cloud_Computing_slides.pdf - Slides for workshop
* gcp-demo - How to use googleComputeEngineR to create and access a GCP VM