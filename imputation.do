//An example of using Stata MP to increase speed with parallel processing.
//This uses the 2016 ACS, a 2.5GB file.
//The data are available on the shared drive for the DuPRI VM.
//m=1 imputations take about 250 seconds on the DuPRI VM and 550 seconds on a MacBook Pro.

clear all
set more off

//Note: you must mount the drive first for this to work.
//Be sure to either mount it as Z or change the drive name to match how you mounted.
cd Z:/csv_pus 

//Importing and destringing takes a long time.
import delimited acs2016.csv
destring rt-pwgtp80, ignore("NA") replace

//Doing the set up to the data set and declaring the variables to be imputed
mi set wide
mi describe
mi misstable summarize jwmnp agep eng jwrip schl sex pincp racblk
mi misstable pattern jwmnp agep eng jwrip schl sex pincp racblk
mi register imputed jwmnp agep eng jwrip schl sex pincp racblk

//Performing the imputation
timer clear 1
timer on 1
mi impute chained (regress) jwmnp (regress) agep (ologit) eng (regress) jwrip (regress) schl (logit) sex (regress) pincp (logit) racblk, add(5)
timer off 1
timer list 1

//Running a regression using the imputed data
timer clear 1
timer on 1
mi estimate: regress jwmnp agep eng jwrip schl sex pincp racblk
timer off 1
timer list 1
