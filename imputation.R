# Showing how parallelization can speed up multiple imputation, then running two models from the imputed data and performing a likelihood ratio test
# Code is benchmarked using the 12-core DuPRI VM

rm(list = ls(all = TRUE))

# loading the required packages
library(haven)
library(parallel)
library(foreach)
library(lmtest)
library(mice)
library(VIM)
library(dplyr)
library(car)

data <- read_stata("GSS2016.dta") # read in the data

#### DATA CLEANING ####
summary(data$age) # age
summary(data$educ) # education
summary(data$income16) # income
summary(data$polviews) # ideology (higher = more conservative)
summary(data$helppoor) # is it important to help the poor

# recode sex to a male dummy
table(data$sex)
data$sex[data$sex==2] <- 0
table(data$sex) # sex, dummy (1 = male)

# recode race to create black variable; this won't affect multiple imputation since there is no missingness on this variable
attributes(data$race) # race, 1 = white, 2 = black, 3 = other
table(data$race)
data$black <- car::recode(data$race, "1=0; 2=1; 3=0")
table(data$black)

# recode party ID to create Democrat variable
attributes(data$partyid) # party ID (0-2 = Democrat)
table(data$partyid)
data$dem <- car::recode(data$partyid, "0:2=1; 3:7=0")
table(data$dem)

# recode whether respondent voted in 2012
attributes(data$vote12)
table(data$vote12)
data$voted2012 <- car::recode(data$vote12, "1=1; 2:3=0")
table(data$voted2012)

# recode spanking variable so it is in the correct direction
attributes(data$spanking)
table(data$spanking)
data$spank <- car::recode(data$spanking, "1=4; 2=3; 3=2; 4=1")
table(data$spank)

# Only choose variables I care about to do the imputation.
gss <- select(data, age, sex, educ, income16, dem, polviews, black, helppoor, spank, voted2012)
objects(gss)


#### MULTIPLE IMPUTATION ####
md.pattern(gss) # examine the pattern of missing data
aggr_plot <- aggr(gss, col=c('navyblue','red'), numbers=TRUE, sortVars=TRUE, labels=names(gss), cex.axis=.7, gap=3, ylab=c("Histogram of missing data","Pattern")) # plot the pattern of missing data

# Since this takes so long, we'll use the parallelized version below instead.
m <- 500 # set the number of imputed data sets
ptm <- proc.time() # start the timer
gss_imp <- mice(gss, m, seed = 984150) # perform the imputation
proc.time() - ptm # report the time
# m = 5: 5 seconds
# m = 10: 9 seconds
# m = 50: 45 seconds
# m = 500: 450 seconds


#### IMPUTATION IN PARALLEL ####
# Alternatively, I can do the imputation in parellel! This speeds things up.
# This code uses socket clusters and can be run on Windows.
no_cores <- detectCores() - 1 # the number of cores I will use
cl <- makeCluster(no_cores) # makes a cluster for parallel processing
clusterSetRNGStream(cl, 984150) # sets the seed so this can be reproduced
clusterExport(cl, "gss") # make sure our data set is loaded onto each core
clusterEvalQ(cl, library(mice)) # make sure package is loaded onto each core

ptm <- proc.time() # start the timer
imp_datasets <- parLapply(cl = cl, X = 1:no_cores, function(x) mice(gss, m = 5)) # creates m * cl imputed data sets; we have 11 clusters
proc.time() - ptm # report the time
stopCluster(cl)
# Parallelized code is benchmarked using the 12-core DuPRI VM
# m = 11: 2 seconds
# m = 55: 8 seconds
# m = 495: 63 seconds

# Now I have to take the list of imputed data sets and bind them together into one object that I can do stuff with.
imp_merged <- imp_datasets[[1]]
for (n in 2:length(imp_datasets)){
  imp_merged <- ibind(imp_merged, imp_datasets[[n]])
}

# # Alternative code using forking that only works on Unix machines
# no_cores <- detectCores() - 1 # number of computing cores for parallel processing
# set.seed(984150, "L'Ecuyer")
# ptm <- proc.time()
# imp_datasets <- mclapply(1:no_cores, function(x) mice(gss, m = 3), mc.cores = no_cores)
# proc.time() - ptm
# # Now I have to take the list of imputed data sets and bind them together into one object that I can do stuff with.
# imp_merged <- imp_datasets[[1]]
# for (n in 2:length(imp_datasets)){
#   imp_merged <- ibind(imp_merged, imp_datasets[[n]])
# }


#### LOOKING AT THE DATA ####
# Taking a look at some of the imputed data
summary(imp_merged$imp[[1]][1:5]) # age
summary(imp_merged$imp[[2]][1:5]) # sex
summary(imp_merged$imp[[3]][1:5]) # education
summary(imp_merged$imp[[4]][1:5]) # income
summary(imp_merged$imp[[5]][1:5]) # party ID
summary(imp_merged$imp[[6]][1:5]) # ideology
summary(imp_merged$imp[[7]][1:5]) # important to help the poor?
summary(imp_merged$imp[[8]][1:5]) # voted in 2012?
summary(imp_merged$imp[[9]][1:5]) # race
summary(imp_merged$imp[[10]][1:5]) # is spanking okay?

#### RUNNING THE TWO MODELS ####
# The with function is part of the mice package and allows us to run our regression on each of the imputed data sets
model1 <- with(imp_merged, glm(voted2012 ~ age + sex + black + educ + income16 + dem + polviews + helppoor + spank, family = binomial(link = "logit")))
summary(model1$analyses[[1]])

model2 <- with(imp_merged, glm(voted2012 ~ age + sex + black + educ + income16 + dem + polviews, family = binomial(link = "logit")))
summary(model2$analyses[[1]])

# Doing the likelihood ratio test on the first of m regressions
model2$analyses[[1]]$deviance - model1$analyses[[1]]$deviance
lrtest(model1$analyses[[1]], model2$analyses[[1]])


#### CALCULATE THE CHI-SQUARE VALUES FOR EACH IMPUTED DATA SET MODEL 3 DIFFERENT WAYS ####
chi_sq <- NA
p_value <- NA
num <- length(model1$analyses)
# Loop that extracts the chi-square values from the likelihood ratio test for each of the imputed data sets
ptm <- proc.time() # start the timer
for (i in 1:num) {
  chi_sq[i] <- lrtest(model1$analyses[[i]], model2$analyses[[i]])[2,4]
  p_value[i] <- lrtest(model1$analyses[[i]], model2$analyses[[i]])[2,5]
}
proc.time() - ptm # report the time
mean(chi_sq)
sd(chi_sq)
plot(density(chi_sq))

# Another way of performing the loop above using foreach instead of for
ptm <- proc.time() # start the timer
alternate <- foreach(i=1:num, .combine = rbind) %do% {
  chi_sq <- lrtest(model1$analyses[[i]], model2$analyses[[i]])[2,4]
  p_value <- lrtest(model1$analyses[[i]], model2$analyses[[i]])[2,5]
  cbind(chi_sq, p_value)
}
proc.time() - ptm # report the time
mean(alternate[,1])
sd(alternate[,1])
plot(density(alternate[,1]))

# Extracting chi-square and p-values using a vectorized function instead of a loop. This is probably the most elegant/efficient way of doing this.
ptm <- proc.time() # start the timer
chi_sq <- sapply(1:num, function(x) chi_sq <- lrtest(model1$analyses[[x]], model2$analyses[[x]])[2,4])
p_value <- sapply(1:num, function(x) chi_sq <- lrtest(model1$analyses[[x]], model2$analyses[[x]])[2,5])
proc.time() - ptm # report the time
mean(chi_sq)
sd(chi_sq)
plot(density(chi_sq))





